#ifndef __GAMELIBRARY_H__
#define __GAMELIBRARY_H__

#include "Header.h"
#include "CDevice.h"
#include "CFpsController.h"
#include "CGame.h"
#include "Config.h"
#include "CState.h"
#include "CStateManagement.h"
#include "CViewController.h"
#include "CVSView.h"
#include "SConfig.h"
#include "Utils.h"
#include "CControllerEventManager.h"
#include "CLookupTableStr.h"
#include "CLookupTableI.h"
#if (CONFIG_PLATFORM==PLATFORM_WIN32)
#include "CAudioPlayer.h"
#endif
#include "Vertex.h"
#include "CShader.h"
#include "Texture.h"
#include "CMath.h"

using namespace GameLibrary;
extern SGameConfig Configuation;

#endif