#include "CAudioDriver.h"

namespace GameLibrary
{
	CAudioDriver::CAudioDriver(EAudioDriver driver) : m_AudioDriver(driver)
	{}

	bool CAudioDriver::IsAudioDriver(EAudioDriver driver)
	{
		return (m_AudioDriver == driver);
	}
}