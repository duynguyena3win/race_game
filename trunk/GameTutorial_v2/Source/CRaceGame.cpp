#include "CRaceGame.h"
#include "CLogoState.h"

CRaceGame::CRaceGame() : CGame()
{
	Init();
}

CRaceGame::~CRaceGame()
{

}

bool CRaceGame::OnControllerEvent(SControllerEvent Event)
{
	if (Event.Type == ECE_KEY &&
		Event.KeyData.Event == EKE_PRESSED &&
		Event.KeyData.KeyCode == EKEY_WIN_Q)
	{
		this->Exit();
		return true;
	}
	return false;
}

void CRaceGame::Init()
{
	CStateManagement::GetInstance()->SwitchState(new CLogoState());
	Log("Init - Race Game");
}

void CRaceGame::Destroy()
{
	Log("Destroy - Race Game");
}