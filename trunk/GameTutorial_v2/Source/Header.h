#ifndef __HEADER_H__
#define __HEADER_H__

#define PLATFORM_WIN32 1
#define PLATFORM_ANDROID 2

#include "Config.h"

#include "Macros.h"

// Include for OpenGL library
#if (CONFIG_PLATFORM==PLATFORM_WIN32)


#pragma comment(lib, "libEGL.lib")
#pragma comment(lib, "libGLESv2.lib")

#include <windows.h>
#include <time.h>

#include <GLES2\gl2.h>
#include <EGL\egl.h>

#pragma comment(lib, "OpenAL32.lib")
#include <al.h>
#include <alc.h>

#	pragma comment(lib, "libogg_static.lib")
#	pragma comment(lib, "libvorbis_static.lib")
#	pragma comment(lib, "libvorbisfile_static.lib")

#	include <codec.h>
#	include <ogg.h>
#	include <os_types.h>
#	include <vorbisenc.h>
#	include <vorbisfile.h>

#elif (CONFIG_PLATFORM==PLATFORM_ANDROID)
#include <unistd.h>
#include <time.h>

#include <GLES2/gl2.h>
#include <EGL/egl.h>
#endif

// Config for View_Class
#if (CONFIG_PLATFORM==PLATFORM_WIN32)
#define	VIEWCLASS	CVSView
#else
#define VIEWCLASS	CVSView
#endif

// Integer type redefine
typedef unsigned long long	__UINT64;
typedef long long			__INT64;
typedef int					__INT32;
typedef unsigned int		__UINT32;
typedef signed short		__INT16;
typedef unsigned short		__UINT16;
typedef signed char			__INT8;
typedef unsigned char		__UINT8;

#endif