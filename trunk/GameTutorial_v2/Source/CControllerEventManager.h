#ifndef _CCONTROLLEREVENTMANAGER_H_
#define _CCONTROLLEREVENTMANAGER_H_

#include "Header.h"
#include "CLookupTableI.h"
#include "CSingleton.h"


namespace GameLibrary
{
	class CControllerEventManager;


#include "CControllerPointerManager.h"
#include "CControllerKeyManager.h"

	class CControllerEventManager : public CSingleton<CControllerEventManager>
	{
		friend class CSingleton < CControllerEventManager > ;
	protected:
		CControllerEventManager();
	public:
		virtual ~CControllerEventManager();
		void OnEvent(SControllerEvent Event);
		void Update();
	private:
		void OnPointerEvent(SControllerEventPointerData Event);
		void OnKeyEvent(SControllerEventKeyData Event);
		CControllerPointerManager* m_pPointerEventManager;
		CControllerKeyManager* m_pKeyEventManager;
	};
}
#endif