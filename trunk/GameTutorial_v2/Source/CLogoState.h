#ifndef __CLOGOSTATE_H__
#define __CLOGOSTATE_H__

#include "GameLibrary.h"

using namespace GameLibrary;

class CLogoState : public CState
{
private:
	__INT32 m_iCount;

	// Just For Example
	Vertex Vertices[6];
	GLuint mVBO;
	CShader* myShader;
	Texture* imageBackground;
	//-----------------
public:
	CLogoState();
	~CLogoState();

	void Init();
	void Render();
	void Update();
	void Exit();
	void OnControllerEvent(SControllerEvent Event);
};

#endif