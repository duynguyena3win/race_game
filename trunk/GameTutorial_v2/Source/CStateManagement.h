#ifndef _CSTATEMANAGEMENT_H_
#define _CSTATEMANAGEMENT_H_
#include "CState.h"
#include "CSingleton.h"

namespace GameLibrary
{
	class CStateManagement : public CSingleton<CStateManagement>
	{
		friend class CSingleton<CStateManagement>;
	protected:
		CState* m_pCurrentState;
		CState* m_pNextState;
	protected:
		CStateManagement() : m_pCurrentState(0), m_pNextState(0) { }
	public:
		void Update(bool isPause);
		void SwitchState(CState* nextState);
		CState* GetCurrentState() { return m_pCurrentState; }
	};
}
#endif