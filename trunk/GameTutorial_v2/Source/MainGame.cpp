#include "GameLibrary.h"
#include "CRaceGame.h"

using namespace GameLibrary;

int main()
{
	SGameConfig cnf = {
		800,
		600,
		false,
		"Hello",
		0,
		CALAudioDriver::GetInstance(),
		new CRaceGame(),
	};

	GameLibrary::StartApp(cnf);
	GameLibrary::FinalizeApp();
	return true;
}