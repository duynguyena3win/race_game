#ifndef __CVSVIEW_H__
#define __CVSVIEW_H__

#include "Header.h"
#include "CSingleton.h"

namespace GameLibrary
{
	class CVSView : public CAbsSingleton<CVSView>
	{
	public:
		CVSView(__INT32 w, __INT32 h, bool fullscreen = false, const char*name = 0);
		virtual ~CVSView();
		void Update();
		__INT32 GetWidth() { return m_iWidth; }
		__INT32 GetHeight() { return m_iHeight; }
		__INT32 IsFullScreen() { return m_isFullScreen; }

	private:
		__INT32 m_iWidth;
		__INT32 m_iHeight;
		bool m_isInit;
		bool m_isFullScreen;
		char *m_strTitle;
#if (CONFIG_PLATFORM==PLATFORM_WIN32)
		void InitClientWindow(int width, int height);
#endif
		void Destroy();
		void SwapBuffers();

#if (CONFIG_PLATFORM==PLATFORM_WIN32)
	private:	//For Win32
		//config for win32
		MSG sMessage;

		// Windows variables
		HWND				nativeWindow;
		HDC					deviceContext;

		// EGL variables
		EGLDisplay			eglDisplay;
		EGLConfig			eglConfig;
		EGLSurface			eglSurface;
		EGLContext			eglContext;

	public:	// For Win32
		//win32 function
		static LRESULT CALLBACK ProcessWindow(HWND hWnd, UINT uiMsg, WPARAM wParam, LPARAM lParam);
		static RECT GetLocalCoordinates(HWND hWnd);

		static bool CreateWindowAndDisplay(int width, int height, HWND &nativeWindow, HDC &deviceContext);
		static bool CreateEGLDisplay(HDC deviceContext, EGLDisplay &eglDisplay);
		static bool ChooseEGLConfig(EGLDisplay eglDisplay, EGLConfig& eglConfig);
		static bool CreateEGLSurface(HWND nativeWindow, EGLDisplay eglDisplay, EGLConfig eglConfig, EGLSurface& eglSurface);
		static bool SetupEGLContext(EGLDisplay eglDisplay, EGLConfig eglConfig, EGLSurface eglSurface, EGLContext& eglContext, HWND nativeWindow);
#endif
	private:
		bool m_isLeftMouseDown;
		bool m_isKeyDown;
	};
}

#endif