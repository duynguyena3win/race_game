#include "Texture.h"
#include "TGA.h"

GLuint Texture::Load2DTexture(char* textureName)
{
	return Load2DTextures(&textureName, 1)[0];
}

GLuint Texture::Load2DTextureAnpha(char* textureName)
{
	return Load2DTextures(&textureName, 1)[0];
}

GLuint* Texture::Load2DTextures(char** listTextureNames, int iCount)
{
	m_pTextureTarget = GL_TEXTURE_2D;
	//Set textures count
	m_iCount = iCount;

	//Init list textures id
	if (!m_iIdTextures)
		SAFE_DEL(m_iIdTextures);
	m_iIdTextures = new GLuint[m_iCount];

	//Generate Texture Buffer
	glGenBuffers(m_iCount, m_iIdTextures);

	//Binding texture data
	for (int i = 0; i < m_iCount; i++)
	{
		//Texture info
		int iWidth, iHeight, iBPP;
		char* imageData = NULL;
		GLenum eTextureFormat;

		//Load texture data from TGA file
		imageData = LoadTGA(listTextureNames[i], &iWidth, &iHeight, &iBPP);
		switch (iBPP)
		{
		case 24:
			eTextureFormat = GL_RGB;
			break;
		case 32:
			eTextureFormat = GL_RGBA;
			break;
		default:
			SAFE_DEL(m_iIdTextures);
			SAFE_DEL(imageData);
			return NULL;
		}

		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(m_pTextureTarget, m_iIdTextures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, eTextureFormat, iWidth, iHeight, 0, eTextureFormat, GL_UNSIGNED_BYTE, imageData);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(m_pTextureTarget, 0);
		SAFE_DEL(imageData);
	}

	return m_iIdTextures;
}

GLuint Texture::LoadTextureCube(char** listTextureNames)
{
	return NULL;
}

void Texture::Bind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(m_pTextureTarget, m_iIdTextures[0]);
}

void Texture::UnBind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(m_pTextureTarget, 0);
}

Texture::Texture(char* textureNames) : m_iCount(0)
{
	Load2DTexture(textureNames);
}

Texture::~Texture()
{
	SAFE_DEL(m_iIdTextures);
}