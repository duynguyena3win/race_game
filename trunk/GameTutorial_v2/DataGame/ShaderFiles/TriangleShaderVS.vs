attribute vec3 a_posL;

attribute vec2 a_uvL;
varying vec2 v_uvL;

//attribute vec3 a_colorL;
//varying vec4 v_colorL;

void main()
{
	vec4 posL = vec4(a_posL, 1.0);
	gl_Position = posL;
	//v_colorL = vec4(a_colorL, 1.0);
	v_uvL = a_uvL;
}
   