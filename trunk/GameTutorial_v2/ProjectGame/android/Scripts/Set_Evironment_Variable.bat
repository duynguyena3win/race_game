ECHO OFF

rem *************************************
rem **** Set environment for Android ****
rem *************************************
SET PROJECT_DIR=E:\Race_Game_Tut\trunk\GameTotorial_v2\ProjectGame\android
SET TARGET_SDK=10
SET TARGET_ANDROID=17
SET NAME_PROJECT=RaceGame
SET NAME_PACKAGES=com.duynguyenigc.racegame

rem ***********************************
rem **** Set environment for Build ****
rem ***********************************

set JAVA_HOME=Y:\jdk1.6.0
set ANDROID_SDK_HOME=Y:\android-sdk-windows
set CYGWIN_BIN=Y:\cygwin\bin

set APACHE_ANT=Y:\apache-ant-1.8.0\bin
set PYTHON_HOME=Y:\Python26\
set PYTHON=%PYTHON_HOME%\python.exe
set NDK_BUILD=Y:\android-ndk-r9d\ndk-build