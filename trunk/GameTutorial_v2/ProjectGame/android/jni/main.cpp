#include "MainJNI.h"
#include "../../../Source/GameLibrary.h"
#include "CRaceGame.h"

using namespace GameLibrary;

JNIEXPORT void 
JNICALL Java_com_duynguyenigc_racegame_NativeLib_nativeInit(JNIEnv *env, jclass javaThis, jint width, jint height)
{
	SGameConfig cnf = {
			width,
			height,
			false,
			"Hello",
			0,
			NULL,
			new CRaceGame(),
		};
		
	GameLibrary::StartApp(cnf);
}


JNIEXPORT void 
JNICALL Java_com_duynguyenigc_racegame_NativeLib_nativeRender(JNIEnv *env, jclass javaThis)
{
	CViewController<VIEWCLASS>::GetInstance()->GetView()->Update();
}

JNIEXPORT void 
JNICALL Java_com_duynguyenigc_racegame_NativeLib_nativeResize(JNIEnv *env, jclass javaThis, jint width, jint height)
{
}