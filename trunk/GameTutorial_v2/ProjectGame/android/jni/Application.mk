APP_ABI := armeabi-v7a armeabi

APP_STL += gnustl_static
APP_STL := gnustl_shared

APP_PLATFORM=android-10

APP_CPPFLAGS := -frtti
APP_CPPFLAGS += -fexceptions \
				-DOS_ANDROID \
				-D_ANDROID
NDK_TOOLCHAIN_VERSION := 4.8