precision mediump float;

uniform sampler2D u_texture;
varying vec2 v_uvL;

//varying vec4 v_colorL;

void main()
{
	gl_FragColor = texture2D(u_texture, v_uvL);
}
