#include "CLogoState.h"

CLogoState::CLogoState() : m_iCount(0), CState()
{
}

CLogoState::~CLogoState()
{

}

void CLogoState::Init()
{
	Log("State Logo: Init");
	m_iCount = 0;
	
	Vertices[0].m_vPos = Vector3(-1.0f, 1.0f, 0.0f);
	Vertices[1].m_vPos = Vector3(1.0f, 1.0f, 0.0f);
	Vertices[2].m_vPos = Vector3(-1.0f, -1.0f, 0.0f);

	Vertices[3].m_vPos = Vector3(1.0f, 1.0f, 0.0f);
	Vertices[4].m_vPos = Vector3(-1.0f, -1.0f, 0.0f);
	Vertices[5].m_vPos = Vector3(1.0f, -1.0f, 0.0f);

	//Vertices[0].m_vColor = Vector3(1.0f, 0.0f, 0.0f);		// Red
	//Vertices[1].m_vColor = Vector3(0.0f, 1.0f, 0.0f);		// Green
	//Vertices[2].m_vColor = Vector3(0.0f, 0.0f, 1.0f);		// Blue

	Vertices[0].m_vUV = Vector2(0.0f, 0.0f);
	Vertices[1].m_vUV = Vector2(1.0f, 0.0f);
	Vertices[2].m_vUV = Vector2(0.0f, 1.0f);

	Vertices[3].m_vUV = Vector2(1.0f, 0.0f);
	Vertices[4].m_vUV = Vector2(0.0f, 1.0f);
	Vertices[5].m_vUV = Vector2(1.0f, 1.0f);

	// Use VBO save vertices
	// Init buffer for Vertices[3]
	
	glGenBuffers(1, &mVBO);

	// Jump to buffer with address mVBO
	glBindBuffer(GL_ARRAY_BUFFER, mVBO);	

	// Write Data in Vertices[3] to buffer with address mVBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 6, Vertices, GL_STATIC_DRAW);

	// Jump to head of Buffer -- noted: always do it after do something with buffer.
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	imageBackground = new Texture("DataGame\\Images\\logoCar.tga");
	myShader = new CShader("DataGame\\ShaderFiles\\TriangleShaderVS.vs", "DataGame\\ShaderFiles\\TriangleShaderFS.fs");
}

void CLogoState::Render()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(myShader->GetProgram());

	glBindBuffer(GL_ARRAY_BUFFER, mVBO);

	GLuint pos = myShader->GetAttribute("a_posL");
	if (pos != -1)
	{
		glEnableVertexAttribArray(pos);
		glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	}

	GLuint uv = myShader->GetAttribute("a_uvL");
	if (uv != -1)
	{
		glEnableVertexAttribArray(uv);
		glVertexAttribPointer(uv, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)12);
	}

	//GLuint color = myShader->GetAttribute("a_colorL");
	//if (color != -1)
	//{
	//	glEnableVertexAttribArray(color);
	//	glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)12);
	//}


	GLuint texture = myShader->GetUniform("u_texture");
	if (texture != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		imageBackground->Bind(GL_TEXTURE0);
	}

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	Log("State Logo: %d Fps = %d FrameDT = %d", m_iCount, 
		CFpsController::GetInstance()->GetRuntimeFps(), 
		CFpsController::GetInstance()->GetFrameDt());
}

void CLogoState::Update()
{
	
}

void CLogoState::Exit()
{
	Log("State Logo: Exit");
}

void CLogoState::OnControllerEvent(GameLibrary::SControllerEvent Event)
{

}