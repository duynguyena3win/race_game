#ifndef __CRACEGAME_H_
#define __CRACEGAME_H_

#include "GameLibrary.h"

using namespace GameLibrary;

class CRaceGame : public CGame
{
public:
	CRaceGame();
	virtual ~CRaceGame();

	virtual bool OnControllerEvent(SControllerEvent Event);
protected:
	void Init();
	void Destroy();
};

#endif