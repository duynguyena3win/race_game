#include "GameLibrary.h"
#include "CRaceGame.h"

using namespace GameLibrary;

int main()
{
	glViewport(0, 0, 800, 600);
	SGameConfig cnf = {
		800,
		600,
		false,
		"Hello",
		0,
		CALAudioDriver::GetInstance(),
		new CRaceGame(),
	};
	GameLibrary::StartApp(cnf);
	GameLibrary::FinalizeApp();
	return true;
}