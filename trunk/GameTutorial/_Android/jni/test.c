#include <jni.h>
#include "MainJNI.h"

JNIEXPORT jstring JNICALL Java_com_duynguyen_racegame_MainActivity_hello
    (JNIEnv *env, jobject javaThis) {
    return (*env)->NewStringUTF(env, "Hello World!");
}

  JNIEXPORT jint JNICALL Java_com_duynguyen_racegame_MainActivity_add
      (JNIEnv *env, jobject javaThis, jint value1, jint value2){
return (value1 + value2);
    }