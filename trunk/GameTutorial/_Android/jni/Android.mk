LOCAL_PATH := $(call my-dir)

# Build main lib
include $(CLEAR_VARS)

LOCAL_CFLAGS := -DOS_ANDROID \
				-D_ANDROID \
				-UNDEBUG 
           
LOCAL_MODULE := racegame
LOCAL_MODULE_FILENAME := racegame

LOCAL_LDLIBS	:= -llog -lGLESv2 -lEGL -landroid
LOCAL_CFLAGS	:= 	-mandroid \
					-ffunction-sections \
					-funwind-tables \
					-fstack-protector \
					-fpic \
					-Wno-psabi \
					-mfpu=vfpv3-d16 \
					-mfloat-abi=softfp \
					-Wno-write-strings \
					-fsigned-char \
					-fomit-frame-pointer \
					-fno-strict-aliasing \
					-finline-limit=300 \
					-ffast-math \
					-pipe \
					-g \
					-w -O3 \
					-fno-rtti \
                    -fexceptions
					
LOCAL_LDFLAGS	:= -Wl,--allow-multiple-definition

LOCAL_C_INCLUDES := HelloJNI.h \
					$(LOCAL_PATH)/../../Source

LOCAL_SRC_FILES :=	main.c \
					../../../Source/CRaceGame.cpp \
					../../../Source/CLogoState.cpp \
					../../../Source/CDevice.cpp \
					../../../Source/CFpsController.cpp \
					../../../Source/CGame.cpp \
					../../../Source/CState.cpp \
					../../../Source/CStateManagement.cpp \
					../../../Source/CViewController.cpp \
					../../../Source/CVSView.cpp \
					../../../Source/CControllerEventManager.cpp \
					../../../Source/CAudioPlayer.cpp \
					../../../Source/CShader.cpp \
					../../../Source/Texture.cpp 
													
					
LOCAL_WHOLE_STATIC_LIBRARIES := 					

include $(BUILD_SHARED_LIBRARY)