package com.duynguyen.racegame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Bundle;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MainActivity extends Activity
{
	public String Tag="Race Game";
	
	static MainActivity		mInstance = null;
	
	static GameSurfaceView 	mView = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        mInstance = this;
        mView = new GameSurfaceView(getApplication());
        //setContentView(mView);
        setContentView(R.layout.layout);
        TextView text = (TextView) findViewById(R.id.textView1);
        text.setText("Hello!");
        SoundManger();
    }
    
    void SoundManger()
    {
    	AudioManager myAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener()
        {

			@Override
			public void onAudioFocusChange(int focusChange) {
				TextView text = (TextView) findViewById(R.id.textView1);
				switch(focusChange)
				{ 
				case AudioManager.AUDIOFOCUS_GAIN:
					text.setText("AUDIOFOCUS_GAIN");
					break;
				case AudioManager.AUDIOFOCUS_LOSS:
					text.setText("AUDIOFOCUS_LOSS");
					break;
				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
					text.setText("AUDIOFOCUS_LOSS_TRANSIENT");
					break;
				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
					text.setText("AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
					break;
				}
			}
        	
        };
    }
    @Override protected void onPause() {
        super.onPause();
        mView.onPause();
        
    }

    @Override protected void onResume() {
        super.onResume();
        mView.onResume();
        AudioManager myAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        TextView text = (TextView) findViewById(R.id.textView1);
        if(myAudioManager.isMusicActive())
        	text.setText("ON");
        else
        	text.setText("OFF");
    }
    
    static
    {
    	System.loadLibrary("racegame");
    } 
   
}
