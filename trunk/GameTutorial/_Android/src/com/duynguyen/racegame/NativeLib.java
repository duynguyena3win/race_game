package com.duynguyen.racegame;

public class NativeLib {
	public native String hello();
	public static native void nativeInit(int m_iWidth, int m_iHeight);
	public static native void nativeRender();
	public static native void nativeResize(int width, int height);
}
