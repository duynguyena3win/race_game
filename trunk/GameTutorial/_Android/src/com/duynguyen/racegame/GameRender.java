package com.duynguyen.racegame;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.annotation.SuppressLint;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.Display;

@SuppressLint("NewApi")
public class GameRender implements GLSurfaceView.Renderer {

	int m_iWidth;
	int m_iHeight;
	
	GameRender()
	{
		Display display = MainActivity.mInstance.getWindowManager().getDefaultDisplay();
		m_iWidth = display.getWidth();
		m_iHeight = display.getHeight();
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		Log.i("GAME", "Java - Init game");
		NativeLib.nativeInit(m_iWidth, m_iHeight);
		Log.i("GAME", "Init Done game");
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		NativeLib.nativeResize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		NativeLib.nativeRender();
	}

}
