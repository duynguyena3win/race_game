#include "Header.h"

enum EControllerEvent
{
	ECE_POINTER,
	ECE_KEY
};

enum EPointerEvent
{
	EPE_NONE,
	EPE_PRESSED,
	EPE_RELEASED,
	EPE_DRAGGED
};

enum EKeyEvent
{
	EKE_NONE,
	EKE_PRESSED,
	EKE_HOLD,
	EKE_RELEASED
};

typedef struct _SControllerEventPointerData
{
	__INT32 ID;
	__INT32 X;
	__INT32 Y;
	EPointerEvent Event;
} SControllerEventPointerData;

typedef struct _SControllerEventKeyData
{
	__INT32 KeyCode;
	EKeyEvent Event;
} SControllerEventKeyData;

typedef struct _SControllerEvent
{
	EControllerEvent Type;

	union
	{
		SControllerEventPointerData PointerData;
		SControllerEventKeyData KeyData;
	};

} SControllerEvent;