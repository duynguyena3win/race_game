#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "Header.h"

// Specify Game Platform
// Values:
//		PLATFORM_WIN32	: for Window Visual
//		PLATFORM_ANDROID : for Android 
#define CONFIG_PLATFORM PLATFORM_WIN32

#define BUFFER_BIG_SIZE 512

#endif