#ifndef __SCONFIG_H__
#define __SCONFIG_H__

#include "Header.h"

namespace GameLibrary
{
	class CAudioDriver;
	class CGame;
	class IVideoDriver;

	typedef struct _SGameConfig
	{
		__UINT16 iWidth;
		__UINT16 iHeight;
		bool isFullScreen;
		const char* strTitle;
		IVideoDriver *pVideoDriver;
		CAudioDriver *pAudioDriver;
		CGame* pGame;
	} SGameConfig;
}

#endif