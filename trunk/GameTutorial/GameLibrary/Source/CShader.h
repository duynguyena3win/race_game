#ifndef __CSHADER_H__
#define __CSHADER_H__

#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

class CShader
{
public:
	GLuint GetProgram();
	GLuint GetAttribute(const char *attributeName);
	GLuint GetUniform(const char *uniformName);

	bool InitShader(char* fileVertexShader, char* fileFragmentShader);
	
	friend GLuint ESUTIL_API esLoadShader(GLenum type, char * filename);
	friend GLuint ESUTIL_API esLoadProgram(GLuint vertexShader, GLuint fragmentShader);

	CShader();
	CShader(char* fileVertexShader, char* fileFragmentShader);
	virtual ~CShader();
protected:
	GLuint m_iProgram;
	GLuint m_iVertexShader;
	GLuint m_iFragmentShader;
};
#endif