#include "CControllerEventManager_inc.h"
#include "CSingleton.h"
#include "CLookupTableI.h"


typedef struct _SPointerStorageInformation
{
	__INT32 ID;
	EPointerEvent Status;
	__INT32 PressedX;
	__INT32 PressedY;
	__INT32 DraggedX;
	__INT32 DraggedY;
	__INT32 LastDraggedX;
	__INT32 LastDraggedY;
	__INT32 DraggedDiffX;
	__INT32 DraggedDiffY;
} SPointerStorageInformation;

template <class T>
struct SPosition2D
{
	SPosition2D(T x = 0, T y = 0)
	{
		X = x; Y = y;
	}

	T X;
	T Y;
};

typedef struct _SPointerUserInfo		// for user
{
	__INT32 ID;
	SPosition2D<__INT32> OriginalPosition;
	SPosition2D<__INT32> CurrentPosition;
	__INT32 DragDistanceX;
	__INT32 DragDistanceY;
	__INT32 DragDiffDistanceX;
	__INT32 DragDiffDistanceY;
} SPointerUserInfo;

class CControllerPointerManager : public CSingleton<CControllerPointerManager>
{
	friend class CSingleton < CControllerPointerManager > ;
	friend class CControllerEventManager;
protected:
	CControllerPointerManager();
public:
	virtual ~CControllerPointerManager();
	void Reset();
	void SleepUntilPress();
	void Enable(bool value) { m_isEnable = value; }
	bool IsEnable() { return m_isEnable; }
	bool WasTouchedInside(__INT32 x, __INT32 y, __INT32 w, __INT32 h);
	bool WasReleasedInside(__INT32 x, __INT32 y, __INT32 w, __INT32 h);
	__INT32 GetNumberOfActivePointer();
	void GetActivePointer(__INT32 maxPointer, __INT32 &num, SPointerUserInfo* info);
private:
	void PointerPressed(__INT32 ID, __INT32 x, __INT32 y);
	void PointerReleased(__INT32 ID, __INT32 x, __INT32 y);
	void PointerDragged(__INT32 ID, __INT32 x, __INT32 y);
	void Update();
private:
	
	GameLibrary::CLutI<SPointerStorageInformation*> m_PointersInfo;
	bool m_isEnable;
	bool m_isSleeping;
};