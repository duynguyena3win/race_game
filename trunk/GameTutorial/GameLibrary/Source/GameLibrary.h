#ifndef __GAMELIBRARY_H__
#define __GAMELIBRARY_H__

#include "Header.h"
#include "CDevice.h"
#include "CFpsController.h"
#include "CGame.h"
#include "Config.h"
#include "CState.h"
#include "CStateManagement.h"
#include "CViewController.h"
#include "CVSView.h"
#include "SConfig.h"
#include "Utils.h"
#include "CControllerEventManager.h"
#include "CLookupTableStr.h"
#include "CLookupTableI.h"
#include "CAudioPlayer.h"
#include "Vertex.h"
#include "CShader.h"
#include "Texture.h"

#if CONFIG_PLATFORM==PLATFORM_WIN32_VS
#	pragma comment(lib, "gametutor.lib")
#endif

using namespace GameLibrary;
extern SGameConfig Configuation;

#endif