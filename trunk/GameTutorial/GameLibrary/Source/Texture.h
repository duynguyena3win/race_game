#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include "Header.h"

class Texture
{
private:
	GLenum m_pTextureTarget;	// Type of Texture (2D, Skydom, ...)
	GLuint* m_iIdTextures;		// Id of Textures 
	int m_iCount;				// Number of Textures was bind
public:
	GLuint Load2DTexture(char* textureName);
	GLuint Load2DTextureAnpha(char* textureName);

	GLuint* Load2DTextures(char** listTextureNames, int iCount);
	

	GLuint LoadTextureCube(char** listTextureNames);
	void Bind(GLenum TextureUnit);
	void UnBind(GLenum TextureUnit);
	Texture(char* textureNames);
	~Texture();
};

#endif