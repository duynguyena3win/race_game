#include "CDevice.h"
#include "Macros.h"
#include "CViewController.h"
#include "SConfig.h"
#include "CFpsController.h"
#include "CStateManagement.h"
#include "CControllerEventManager.h"

namespace GameLibrary
{
	SGameConfig Configuation;
}
#if CONFIG_PLATFORM==PLATFORM_WIN32
namespace GameLibrary
{
	void CDevice::SleepEx(__UINT64 milisec)
	{
		Sleep(DWORD(milisec));
	}

	__UINT64 CDevice::GetTimer()
	{
		return clock();
	}

	void StartApp(SGameConfig cnf)
	{
		//init view
		Configuation = cnf;
		CViewController<VIEWCLASS>::CreateView(cnf.iWidth, cnf.iHeight, cnf.isFullScreen, cnf.strTitle);
		CViewController<VIEWCLASS>::GetInstance()->GetView()->Update();
	}

	void FinalizeApp()
	{
		SAFE_DEL(Configuation.pGame);
		//SAFE_DEL(Configuation.pVideoDriver);
		CFpsController::FreeInstance();
		CViewController<VIEWCLASS>::FreeInstance();
		//CFileWin32Driver::FreeInstance();
		//CStreamDriverBuffer::FreeInstance();
		CDevice::FreeInstance();
		CStateManagement::FreeInstance();
		//CGraphics2D::FreeInstance();
		//CImageManager::FreeInstance();
		//CSpriteDBManagement::FreeInstance();
		CControllerEventManager::FreeInstance();
		//CAudioPlayer::FreeInstance();
		//CText::FreeInstance();
	}
}

BOOL(WINAPI *doSetConsoleTextAttribute)(HANDLE hConsoleOutput, WORD attr);

inline void *getConsoleFunction(char *name) {
	static HMODULE kernel32 = (HMODULE)0xffffffff;
	if (kernel32 == 0)
	{
		return NULL;
	}
	if (kernel32 == (HMODULE)0xffffffff)
	{
		kernel32 = LoadLibrary("kernel32.dll");
		if (kernel32 == 0)
		{
			return 0;
		}
	}
	return GetProcAddress(kernel32, name);
}

#else // CONFIG_PLATFORM!=PLATFORM_WIN32

namespace GameLibrary
{
	void CDevice::SleepEx(__UINT64 milisec)
	{
		TODO("CDevice::SleepEx for CONFIG_PLATFORM!=PLATFORM_WIN32 is not implement yet !");
	}

	__UINT64 CDevice::GetTimer()
	{
		TODO(" CDevice::GetTimer for CONFIG_PLATFORM!=PLATFORM_WIN32 is not implement yet !");
		return 0;
	}
}
#endif


