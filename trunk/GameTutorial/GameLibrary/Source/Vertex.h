#ifndef __VERTEX_H__
#define __VERTEX_H__

#include "CMath.h"

class Vertex
{
public:
	Vector3 m_vPos;
	//Vector3 m_vColor;
	Vector2 m_vUV;
};

#endif