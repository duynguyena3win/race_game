#include "CShader.h"


CShader::CShader() : m_iProgram(0), m_iVertexShader(0), m_iFragmentShader(0)
{

}


CShader::CShader(char* fileVertexShader, char* fileFragmentShader) : m_iProgram(0), m_iVertexShader(0), m_iFragmentShader(0)
{
	InitShader(fileVertexShader, fileFragmentShader);
}

CShader::~CShader()
{
	if (m_iProgram != 0)
	{
		glDeleteShader(m_iVertexShader);
		glDeleteShader(m_iVertexShader);
		glDeleteProgram(m_iProgram);
	}
}

GLuint CShader::GetProgram()
{
	return m_iProgram;
}

GLuint CShader::GetAttribute(const char *attributeName)
{
	if (m_iProgram != -1)
	{
		return glGetAttribLocation(m_iProgram, attributeName);
	}
	return -1;
}

GLuint CShader::GetUniform(const char *uniformName)
{
	if (m_iProgram != -1)
	{
		return glGetUniformLocation(m_iProgram, uniformName);
	}
	return -1;
}


bool CShader::InitShader(char* fileVertexShader, char* fileFragmentShader)
{
	Log("Load Vertex Shader: %s", fileVertexShader);
	m_iVertexShader = esLoadShader(GL_VERTEX_SHADER, fileVertexShader);

	if (m_iVertexShader == 0)
	{
		Log(" --- Error !\n");
		return false;
	}

	Log("Load Fragment Shader: %s", fileFragmentShader);
	m_iFragmentShader = esLoadShader(GL_FRAGMENT_SHADER, fileFragmentShader);

	if (m_iFragmentShader == 0)
	{
		glDeleteShader(m_iVertexShader);
		Log(" --- Error !\n");
		return false;
	}

	m_iProgram = esLoadProgram(m_iVertexShader, m_iFragmentShader);
	return true;
}

GLuint ESUTIL_API esLoadShader(GLenum type, char * filename)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader(type);

	if (shader == 0)
		return 0;

	// Load the shader source
	FILE * pf;
	if (fopen_s(&pf, filename, "rb") != 0)
		return NULL;
	fseek(pf, 0, SEEK_END);
	long size = ftell(pf);
	fseek(pf, 0, SEEK_SET);

	char * shaderSrc = new char[size + 1];
	fread(shaderSrc, sizeof(char), size, pf);
	shaderSrc[size] = 0;
	fclose(pf);

	glShaderSource(shader, 1, (const char **)&shaderSrc, NULL);
	delete[] shaderSrc;

	// Compile the shader
	glCompileShader(shader);

	// Check the compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		GLint infoLen = 0;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			char* infoLog = new char[infoLen];


			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			// esLogMessage("Error compiling shader:\n%s\n", infoLog);

			delete[] infoLog;
		}

		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

GLuint ESUTIL_API esLoadProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint programObject;
	GLint linked;

	// Create the program object
	programObject = glCreateProgram();

	if (programObject == 0)
		return 0;

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;

		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			char* infoLog = new char[sizeof(char) * infoLen];


			glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
			//esLogMessage("Error linking program:\n%s\n", infoLog);

			delete infoLog;
		}

		glDeleteProgram(programObject);
		return 0;
	}

	return programObject;
}