#include "CStatePoster.h"
#include "TextIndex.h"

CStatePoster::CStatePoster(): CState()
{
}

void CStatePoster::Init()
{
	Log("State Poster: Init");	
	CGraphics2D::GetInstance()->Reset();

	CSpriteDBManagement::GetInstance()->AddSpriteDBFromTextFile<CFileWin32Driver>("test.spr");

	spr = new CSprite(CSpriteDBManagement::GetInstance()->Get("test.spr"));
	if (spr)
	{
		spr->SetAnimation("XYZ", -1);
		spr->SetPosition(SPosition2D<__INT32>(0, 0));
	}
	

	//CAudioPlayer::GetInstance()->Load<CFileWin32Driver>("test.wav");
	//CAudioPlayer::GetInstance()->Load<CFileWin32Driver>("test2.wav");
	//CAudioPlayer::GetInstance()->Play("test.wav", false);
	//CAudioPlayer::GetInstance()->Play("test2.wav", true);

	CAudioPlayer::GetInstance()->Load<CFileWin32Driver>("in_game.ogg");
	CAudioPlayer::GetInstance()->Play("in_game.ogg", false);

	//CText::GetInstance()->Load<CFileWin32Driver>("EN");
	CText::GetInstance()->Load<CFileWin32Driver>("JP");

}

void CStatePoster::Update()
{
	spr->UpdateAnimation(CFpsController::GetInstance()->GetFrameDt());
	int w = VIEWCLASS::GetInstance()->GetWidth();
	int h = VIEWCLASS::GetInstance()->GetHeight();
	if (CControllerPointerManager::GetInstance()->WasReleaseInside(0, 0, w, h))
	{
		spr->ResetAnimation();
	}
	if (CControllerKeyManager::GetInstance()->GetKeyHoldDuration(EKEY_SPACE) > 1000)
	{
		CGame::GetInstance()->Exit();
	}
}

void CStatePoster::OnControllerEvent(SControllerEvent Event)
{
	if (Event.Type == ECE_KEY && Event.KeyData.Event == EKE_PRESSED && Event.KeyData.KeyCode == EKEY_WIN_P)
	{
		CGame::GetInstance()->Pause();
	}

	if (Event.Type == ECE_KEY && Event.KeyData.Event == EKE_PRESSED && Event.KeyData.KeyCode == EKEY_WIN_R)
	{
		CGame::GetInstance()->Resume();
	}
}

void CStatePoster::Render()
{
	CGraphics2D::GetInstance()->Clear(SColor<float>(0, 0, 0, 1));
	spr->RenderAnimation(CGraphics2D::GetInstance());

	CText::GetInstance()->SetCharSpacing(30);
	CText::GetInstance()->DrawString(CGraphics2D::GetInstance(), TXT_HOWAREYOU, 100, 100, ETEXTANCHOR_LEFT, ETEXTANCHOR_TOP);
	CGraphics2D::GetInstance()->Flush();
}

void CStatePoster::Exit()
{
	Log("State Poster: Exit");
	SAFE_DEL(spr);
	CSpriteDBManagement::GetInstance()->Free("test.spr");
	CAudioPlayer::GetInstance()->Stop("test2.wav");
}
