
#include "CStateLogo.h"
#include "CStatePoster.h"

CStateLogo::CStateLogo():m_iCount(0), CState()
{}

void CStateLogo::Init()
{
	Log("State Logo: Init");
	m_iCount = 0;
}

void CStateLogo::Update()
{
	m_iCount++;
	if (m_iCount >= 10)
	{
		CStateManagement::GetInstance()->SwitchState(new CStatePoster());
	}
}

void CStateLogo::Render()
{
	Log("State Logo: %d Fps = %d FrameDT = %d", m_iCount, CFpsController::GetInstance()->GetRuntimeFps(), CFpsController::GetInstance()->GetFrameDt());
}

void CStateLogo::Exit()
{
	Log("State Logo: Exit");
}
