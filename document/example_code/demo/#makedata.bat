@echo off

if not exist "#config.bat" (
	echo ERROR: #config.bat not found
	goto END
)
call #config.bat

if not exist #build (md #build)
if exist #temp (rd /s /q #temp)
md #temp
xcopy /s/q/y .\#data .\#temp\*.* >nul
if not exist "%TOOLPACK_PATH%\%TOOLPACK%" (
    echo ERROR: can not file %TOOLPACK_PATH%\%TOOLPACK%
	goto END
)
copy %TOOLPACK_PATH%\%TOOLPACK% #temp>nul


echo Build Text
cd .\#temp
%JDK%\java -jar -Xmx512M %TOOLPACK% le text.xls
copy /y .\*.ff ..\#build>nul
copy /y .\*.lang ..\#build>nul
copy /y .\*.h %SOURCE_DIR%>nul

echo copy rest
copy /y .\*.wav ..\#build>nul
copy /y .\*.ogg ..\#build>nul
copy /y .\*.tga ..\#build>nul
copy /y .\*.spr ..\#build>nul
copy /y .\*.bin ..\#build>nul

:END
cd %CUR_DIR%