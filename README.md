# README #

This is Create Game with OpenGL ES and C/C++ Tutorial. It will help you 

### Finish tutorial, you will: ###

* Knowledge about Life Cycle of Game with Good Structer.
* Create simple to profession Game with C/C++ and OpenGL ES - graphic.
* Build Game on Win32 - Window PC and Android device.

### Contain ###

* Step by step becomes empty project to Full Project.
* Fix bugs, errors to make game cant run well on your environments and devices.
* Object-Oriented Programming (OOP) in C/C++ to have good structer source code.
* Knowledge about graphic library OpenGL ES and can code it.
* Create simple model, texture and logic in Game.
* Porting game to new platform: Android or more (will update late).

### Contribution guidelines ###

* Feedback for me about bugs, errors or whatever you want to ask.
* Update my base project becomes a full Game.
* Show your game on Facebook group: [ITUS GAME CLUB] https://www.facebook.com/groups/1565143787055528

### I create the tutorial for: ###

* ITUS Game Club Member of University of Sciense Ho Chi Minh City.
* Everyone want to make Game.